# Blood II - The Choosen Widescreen Patch #

This patch makes the game work without any stretching regardless of the aspect ratio.

### Features: ###
* The FOV is Hor+ (the larger the aspect ratio, the more is seen) and can also be set manually.
* The menu backgrounds, loadingscreens and splashscreen scale correctly and dont stretch.
* The crosshairs scale with the screen resolution.
* The cutscenes are anamorphic (they zoom in so that the original aspect is kept).

### Installation: ###
1. Get Visual Studio 6, load the project file, compile the CShell.dll either with "Release" configuration.
   To compile the dll for the Nightmare Addon, use the "Release Addon" configuration.
2. Use Winrez or a similar program to put the CShell.dll into a rez-file.
3. Put the rez-file in the "Custom" folder in the Blood 2 Installation directory.
4. Run the launcher, click on "Display", choose your resolution and click "OK".
5. Click on "Customize", select your rez-file and click on "Add".
6. Make sure that "Always load these rez files" is enabled.
7. Click "OK" and start the game.
8. Enjoy the game!

### Setting the FOV: ###
If you want to set the horizontal FOV to another value than the automatically calculated one:
1. Run the launcher and click on "Advanced"
2. Write „+FovX "value"„ in the command line field (without the outer quotes).
3. Make sure that "Always pass these command-line parameters" is checked and click "OK".

### Known Problems: ###
* The menu items dont scale. It is impossible to scale them because they are drawn directly to the screen, unlike the bitmaps.
* The character creator looks weird. This is because this menu is a mix of background elements which have been scaled properly and control elements that cant be scaled.
 
Thanks to Monolith and Gametoast.
 
If you encounter bugs or have problems either add them as issue to this repository or post in the Releasethread: http://www.wsgf.org/forums/viewtopic.php?f=64&t=27171